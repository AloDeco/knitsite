# Generated by Django 2.2.7 on 2020-05-04 15:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patrons', '0003_auto_20200504_1142'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='nom',
            new_name='name',
        ),
        migrations.RemoveField(
            model_name='pelote',
            name='pelote_color',
        ),
        migrations.RemoveField(
            model_name='pelote',
            name='pelote_name',
        ),
        migrations.RemoveField(
            model_name='pelote',
            name='pelote_number',
        ),
        migrations.AddField(
            model_name='pelote',
            name='color',
            field=models.CharField(default='pull', max_length=200, verbose_name='Colori'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='pelote',
            name='name',
            field=models.CharField(default='', max_length=200, verbose_name='Nom'),
        ),
        migrations.AddField(
            model_name='pelote',
            name='quantity',
            field=models.IntegerField(default=0, verbose_name='Quantité'),
        ),
        migrations.AlterField(
            model_name='patron',
            name='aiguille_size',
            field=models.FloatField(default=0, verbose_name="Taille d'aiguille"),
        ),
        migrations.AlterField(
            model_name='patron',
            name='aiguille_type',
            field=models.CharField(max_length=200, verbose_name="Type d'aiguille"),
        ),
        migrations.AlterField(
            model_name='patron',
            name='name',
            field=models.CharField(max_length=200, verbose_name='Nom'),
        ),
    ]
