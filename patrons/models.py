from django.db import models
from . import settings
from django.core.files.storage import FileSystemStorage
import re


class Patron(models.Model):
	category = models.ForeignKey('Category', on_delete=models.CASCADE, verbose_name="Catégorie")
	name = models.CharField(max_length=200, verbose_name="Nom")
	image = models.ImageField('Photo', null=True, upload_to='img', storage=FileSystemStorage(location=settings.MEDIA_ROOT, base_url=''))

	def __str__(self):
		return self.name

class Pelote(models.Model):
	patron = models.ForeignKey(Patron, null=True, on_delete=models.CASCADE)
	name = models.CharField(max_length=200, default="", verbose_name="Nom")
	quantity = models.IntegerField(default=0, verbose_name="Quantité")
	color = models.CharField(max_length=200, verbose_name="Colori")

	def __str__(self):
		return "Pelote"

class Aiguille(models.Model):
	patron = models.ForeignKey(Patron, null=True, on_delete=models.CASCADE)
	type = models.CharField(max_length=200, verbose_name="Type d'aiguille")
	size = models.FloatField(default=0, verbose_name="Taille d'aiguille")

	def __str__(self):
		return "Aiguille"

class Etape(models.Model):
	patron = models.ForeignKey(Patron, null=True, on_delete=models.CASCADE)
	text = models.TextField('Etape', max_length=200, null=True)
	schema = models.ImageField('Photo', blank=True, null=True, upload_to='img', storage=FileSystemStorage(location=settings.MEDIA_ROOT, base_url=''))

	def __str__(self):
		return "Etape"

class Echantillon(models.Model):
	patron = models.ForeignKey(Patron, null=True, on_delete=models.CASCADE)
	nb_mailles = models.IntegerField(default=0, verbose_name="Nombre de mailles")
	nb_rangs = models.IntegerField(default=0, verbose_name="Nombre de rangs")
	aiguille = models.CharField(max_length=200, default="", verbose_name="Taille d'aiguille")
	point = models.CharField(max_length=300, default="", verbose_name="Type de point")

	def __str__(self):
		return "Echantillon"

class Category(models.Model):
	name = models.CharField(max_length=30)

	def __str__(self):
		return self.name