from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:id>/', views.patron, name='patron'),
    path('pull/', views.pull, name='pull'),
    path('bonnet/', views.bonnet, name='bonnet'),
    path('gant/', views.gant, name='gant'),
    path('echarpe/', views.echarpe, name='echarpe'),
]