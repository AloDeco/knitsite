from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Patron, Category


def index(request):
    list = Patron.objects.all()
    category_list = Category.objects.all()
    context = {'list': list, 'cat':'index', 'category_list':category_list}
    return render(request, 'patrons/index.html', context)

def pull(request):
    list = Patron.objects.filter(category__name__contains="pull")
    category_list = Category.objects.all()
    context = {'cat': "Pull", 'list': list, 'category_list':category_list}
    return render(request, 'patrons/category.html', context)

def bonnet(request):
    list = Patron.objects.filter(category__name__contains="bonnet")
    category_list = Category.objects.all()
    context = {'cat': "Bonnet", 'list': list, 'category_list':category_list}
    return render(request, 'patrons/category.html', context)

def gant(request):
    list = Patron.objects.filter(category__name__contains="gant")
    category_list = Category.objects.all()
    context = {'cat': "Gant", 'list': list, 'category_list':category_list}
    return render(request, 'patrons/category.html', context)

def echarpe(request):
    list = Patron.objects.filter(category__name__contains="echarpe")
    category_list = Category.objects.all()
    context = {'cat': "Echarpe", 'list': list, 'category_list':category_list}
    return render(request, 'patrons/category.html', context)

def patron(request, id):
    patron = get_object_or_404(Patron, id=id)
    category_list = Category.objects.all()
    context = {'cat': patron.category, 'patron': patron, 'category_list':category_list}
    return render(request, 'patrons/patron.html', context)