from django.contrib import admin

# Register your models here.
from .models import Patron, Pelote, Etape, Category, Echantillon, Aiguille

class PeloteInline(admin.TabularInline):
    model = Pelote
    extra = 1
    fields = ['quantity', 'name', 'color']

class AiguilleInline(admin.TabularInline):
    model = Aiguille
    extra = 1
    fields = ['type', 'size']

class EtapeInline(admin.TabularInline):
    model = Etape
    extra = 1

class EchantillonInline(admin.StackedInline):
    model = Echantillon
    fields = ['point', 'aiguille', 'nb_mailles', 'nb_rangs']
    extra=1

class PatronAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Informations',
         {'fields': ['category', 'name', 'image']}),
    ]
    inlines = [PeloteInline, AiguilleInline, EchantillonInline, EtapeInline]
    list_display = ('name', 'category')
    list_filter = ['category']
    search_fields = ['category', 'name']

admin.site.register(Category)
admin.site.register(Patron, PatronAdmin)